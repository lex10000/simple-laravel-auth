# Простая авторизация по GET параметру (ключу)
### Поддержка времени жизни ключа и количества запросов, которые доступны для ключа.
@inspired by https://github.com/ejarnutowski/laravel-api-key
#### Установка:
1) Прописать название пакета в composer.json
```
   "require": {
   ...
   "lex10000/simple-api-auth": "^1.1.0",
   ...
   }
```
2) В секцию repositories добавить путь к git-репозиторию 
```
  "repositories": [
        {
            "type": "gitlab",
            "url": "https://gitlab.com/lex10000/simple-laravel-auth"
        }
    ],
```
3) Выполнить composer update

#### Доступные команды в командной строке: (здесь и далее для краткости: "php artisan simple-api-auth" = "art auth")
1) <b>art auth generate</b>: Запускает генерацию ключа, где последовательно вас попросят указать название ключа, количество дней действия и количество доступных запросов для ключа.
2) <b>art auth list</b>: Выводит список ключей. Т.к. модель использует трейт soft deletes, то можно посмотреть и удаленные ключи, добавив к команде флаг -D
3) <b>art auth activate {НАЗВАНИЕ КЛЮЧА}</b>: активирует ключ по заданному имени
4) <b>art auth deactivate {НАЗВАНИЕ КЛЮЧА}</b>: деактивирует ключ по заданному имени
5) <b>art auth delete {НАЗВАНИЕ КЛЮЧА}</b>: удаляет ключ по заданному имени

#### Использование:
Добавьте middleware <b>\Lex10000\SimpleApiAuth\Http\Middleware\SimpleApiAuthMiddleware </b>на те роуты, которые вы хотите защитить данной авторизацией.
Пример ниже демонстрирует подключение ко всем /api роутам:
```
//App\Http\Kernel.php
 ...
 'api' => [
            'throttle:api',
            \Lex10000\SimpleApiAuth\Http\Middleware\SimpleApiAuthMiddleware::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],
```
