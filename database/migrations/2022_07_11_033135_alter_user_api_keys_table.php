<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_api_keys', function (Blueprint $table) {
            $table->unsignedBigInteger('available_requests')->default(config('simple-api-auth.default_available_requests'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_api_keys', function (Blueprint $table) {
            $table->dropColumn('available_requests');
        });
    }
};
