<?php

namespace Lex10000\SimpleApiAuth;

use Illuminate\Support\Facades\Facade;

/**
 * @method static string getExpirationDate(int $days)
 * @method static string generateKey()
 * @see \Lex10000\SimpleApiAuth\SimpleApiAuth
 */
class SimpleApiAuthFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'simple-api-auth';
    }
}
