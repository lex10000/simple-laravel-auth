<?php

namespace Lex10000\SimpleApiAuth\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Lex10000\SimpleApiAuth\Enums\KeyTypeEnum;
use Lex10000\SimpleApiAuth\Models\SimpleApiAuth;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SimpleApiAuthMiddleware
{
    /**
     * Handle the incoming request
     *
     * @param Request $request
     * @param Closure $next
     * @return \Illuminate\Contracts\Routing\ResponseFactory|mixed|\Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, Closure $next)
    {
        $requestKey = $request->input('apiKey', null);
        $apiKey = SimpleApiAuth::query()->where('apiKey', $requestKey)->first();
        if (
            $apiKey &&
            $apiKey->is_active &&
            (
                (PHP_VERSION_ID > 80100 && $apiKey->type === KeyTypeEnum::ADMIN || PHP_VERSION_ID < 80100 && $apiKey->type === 'admin') ||
                Carbon::make($apiKey->expired_at)->gt(now()) &&
                $apiKey->available_requests > 0
            )
        ) {
            $apiKey->decrement('available_requests');
            return $next($request);
        }

        throw new HttpException(401, 'Unauthorised');
    }
}