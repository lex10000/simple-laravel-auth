<?php

namespace Lex10000\SimpleApiAuth\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use Lex10000\SimpleApiAuth\Models\SimpleApiAuth;
use Lex10000\SimpleApiAuth\SimpleApiAuthFacade;

class GenerateApiKey extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simple-api-auth:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a new API key';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $name = $this->ask('Введите имя для создаваемого ключа');
        $expiredAt = $this->ask('Введите количество дней действия ключа (по умолчанию - '.config('simple-api-auth.default_expiration').')');
        $availableRequests = $this->ask('Введите количество запросов, доступных для ключа (по умолчанию - '.config('simple-api-auth.default_available_requests').')');
        $type = $this->ask('Введите тип ключа (Доступные варианты: default, test, admin, по умолчанию - default');
        $validator = Validator::make(
            compact('name', 'expiredAt', 'availableRequests', 'type'),
            [
                'name'  => ['required', 'string', 'min:3', 'max:254', 'alpha_dash', 'unique:user_api_keys,name'],
                'expiredAt' => ['sometimes', 'nullable', 'numeric', 'min:1', 'max:'.config('simple-api-auth.default_expiration')],
                'availableRequests' => ['sometimes', 'nullable', 'numeric', 'min:1', 'max:'.config('simple-api-auth.default_available_requests')],
                'type' => ['sometimes', 'nullable', 'string', 'in:default,test,admin']
            ],
        );

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }

            return 1;
        }

        $apiKey = SimpleApiAuth::query()->create(
            [
                'name' => $name,
                'expired_at' => !empty($expiredAt) ?
                    SimpleApiAuthFacade::getExpirationDate($expiredAt) :
                    SimpleApiAuthFacade::getExpirationDate(config('simple-api-auth.default_expiration')),
                'available_requests' => $availableRequests ?? config('simple-api-auth.default_available_requests'),
                'type' => $type ?? 'default'
            ]
        );

        $this->info('API Ключ был успешно создан.');
        $this->info('Название: ' . $apiKey->name);
        $this->info('Ключ: '  . $apiKey->apiKey);
        $this->info('Действителен до: '  . $apiKey->expired_at);
        $this->info('Тип: '  . PHP_VERSION_ID > 80100 ? $apiKey->type->value : $apiKey->type);
        $this->info('Количество запросов: '  . $apiKey->available_requests);

        return 0;
    }
}