<?php

namespace Lex10000\SimpleApiAuth\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use Lex10000\SimpleApiAuth\Enums\KeyTypeEnum;
use Lex10000\SimpleApiAuth\Models\SimpleApiAuth;

class ProlongApiKey extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simple-api-auth:prolong {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Prolong an API key by name';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $name = $this->argument('name');
        $expiredAt = $this->ask('Введите количество дней, на которое хотите продлить ключ (по умолчанию - 0)');
        $availableRequests = $this->ask('Введите количество запросов,на которое хотите продлить ключ (по умолчанию - 0)');

        $validator = Validator::make(
            compact('name', 'expiredAt', 'availableRequests'),
            [
                'name'  => ['required', 'string', 'min:3', 'max:254', 'exists:user_api_keys,name'],
                'expiredAt' => ['sometimes', 'nullable', 'numeric', 'min:1', 'max:'.config('simple-api-auth.default_expiration')],
                'availableRequests' => ['sometimes', 'nullable', 'numeric', 'min:1', 'max:'.config('simple-api-auth.default_available_requests')],
            ],
        );

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }

            return 1;
        }


        $key = SimpleApiAuth::query()->where('name', $name)->first();

        if (
            PHP_VERSION_ID > 80100 && $key->type === KeyTypeEnum::TEST ||
            PHP_VERSION_ID < 80100 && $key->type === 'test'
        ) {
            $this->error('Key with type TEST could not be prolong!');
            return 1;
        }

        $key->available_requests += $availableRequests ?? 0;
        $key->expired_at = $key->expired_at->addDays($expiredAt ?? 0);
        $key->save();

        $this->info('Key: ' . $name. 'was successfully prolonged!');
    }
}