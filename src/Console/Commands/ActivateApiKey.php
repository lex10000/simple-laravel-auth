<?php

namespace Lex10000\SimpleApiAuth\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use Lex10000\SimpleApiAuth\Models\SimpleApiAuth;

class ActivateApiKey extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simple-api-auth:activate {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Activate an API key by name';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $name = $this->argument('name');

        $validator = Validator::make(
            compact('name'),
            [
                'name'  => ['required', 'string', 'min:3', 'max:254', 'exists:user_api_keys,name'],
            ],
        );

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }

            return 1;
        }


        $key = SimpleApiAuth::query()->where('name', $name)->first();

        if ($key->is_active) {
            $this->info('Key "' . $name . '" is already active');
            return 0;
        }

        $key->is_active = true;
        $key->save();

        $this->info('Activated key: ' . $name);
    }
}