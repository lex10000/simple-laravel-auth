<?php

namespace Lex10000\SimpleApiAuth\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use Lex10000\SimpleApiAuth\Models\SimpleApiAuth;

class DeleteApiKey extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simple-api-auth:delete {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete an API key by name';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $name = $this->argument('name');

        $validator = Validator::make(
            compact('name'),
            [
                'name'  => ['required', 'string', 'min:3', 'max:254', 'exists:user_api_keys,name'],
            ],
        );

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }

            return 1;
        }

        $confirmMessage = 'Are you sure you want to delete API key \'' . $name . '\'?';

        if (!$this->confirm($confirmMessage)) {
            $this->info('Ключ не был удален, т.к. вы не подтвердили удаление: ' . $name);

            return 0;
        }

        $key = SimpleApiAuth::where('name', $name)->first();
        $key->delete();

        $this->info('Deleted key: ' . $name);
    }
}
