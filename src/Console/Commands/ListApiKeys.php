<?php

namespace Lex10000\SimpleApiAuth\Console\Commands;

use Illuminate\Console\Command;
use Lex10000\SimpleApiAuth\Models\SimpleApiAuth;

class ListApiKeys extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simple-api-auth:list {--D|deleted}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all API Keys';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $keys = $this->option('deleted')
            ? SimpleApiAuth::query()->withTrashed()->orderBy('name')->get()
            : SimpleApiAuth::query()->orderBy('name')->get();

        if ($keys->count() === 0) {
            $this->info('There are no API keys');
            return;
        }

        $headers = ['Name', 'ID', 'Status', 'Expiration Date', 'Available requests', 'Key', 'Type'];

        $rows = $keys->map(function($key) {

            $status = $key->is_active    ? 'active'  : 'deactivated';
            $status = $key->trashed() ? 'deleted' : $status;

            $statusDate = $key->expired_at;

            return [
                $key->name,
                $key->id,
                $status,
                $statusDate,
                $key->available_requests,
                $key->apiKey,
                PHP_VERSION_ID > 80100 ? $key->type->value : $key->type
            ];

        });

        $this->table($headers, $rows);
    }
}
