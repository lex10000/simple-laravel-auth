<?php

namespace Lex10000\SimpleApiAuth\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Lex10000\SimpleApiAuth\Enums\KeyTypeEnum;
use Lex10000\SimpleApiAuth\SimpleApiAuthFacade;

class SimpleApiAuth extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'user_api_keys';

    protected $casts = [
        'expired_at' => 'datetime',
        'type' => PHP_VERSION_ID > 80100 ? KeyTypeEnum::class : 'string'
    ];

    protected $fillable = [
        'expired_at', 'name', 'apiKey', 'is_active','available_requests', 'type'
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function(SimpleApiAuth $model) {
            $model->expired_at = $model->expired_at ?? SimpleApiAuthFacade::getExpirationDate(config('simple-api-auth.default_expiration'));
            $model->apiKey = $model->apiKey ?? SimpleApiAuthFacade::generateKey();
        });
    }
}
