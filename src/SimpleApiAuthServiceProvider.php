<?php

namespace Lex10000\SimpleApiAuth;

use Illuminate\Support\ServiceProvider;
use Lex10000\SimpleApiAuth\Console\Commands\ActivateApiKey;
use Lex10000\SimpleApiAuth\Console\Commands\DeactivateApiKey;
use Lex10000\SimpleApiAuth\Console\Commands\DeleteApiKey;
use Lex10000\SimpleApiAuth\Console\Commands\GenerateApiKey;
use Lex10000\SimpleApiAuth\Console\Commands\ListApiKeys;
use Lex10000\SimpleApiAuth\Console\Commands\ProlongApiKey;
use Lex10000\SimpleApiAuth\Http\Middleware\SimpleApiAuthMiddleware;

class SimpleApiAuthServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {

        /*
         * Optional methods to load your package assets
         */
         $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/simple-api-auth.php' => config_path('simple-api-auth.php'),
            ], 'config');


        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        app('router')->aliasMiddleware('simple-api-auth', SimpleApiAuthMiddleware::class);

        // Registering package commands.
        $this->commands(
            [
                ActivateApiKey::class,
                DeactivateApiKey::class,
                DeleteApiKey::class,
                GenerateApiKey::class,
                ListApiKeys::class,
                ProlongApiKey::class,

            ]
        );

        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/simple-api-auth.php', 'simple-api-auth');

        // Register the main class to use with the facade
        $this->app->singleton('simple-api-auth', function () {
            return new SimpleApiAuth;
        });
    }
}
