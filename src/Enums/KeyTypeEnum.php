<?php

namespace Lex10000\SimpleApiAuth\Enums;

enum KeyTypeEnum: string
{
    case DEFAULT = 'default';
    case TEST = 'test';
    case ADMIN = 'admin';
}