<?php

namespace Lex10000\SimpleApiAuth;

use Illuminate\Support\Str;
use Lex10000\SimpleApiAuth\Models\SimpleApiAuth as SimpleApiAuthModel;

class SimpleApiAuth
{
    /**
     * Generate a secure (very very secure) unique API key
     *
     * @return string
     */
    public function generateKey(): string
    {
        do {
            $key = Str::random(64);
        } while (SimpleApiAuthModel::query()->where('apiKey')->exists());

        return $key;
    }

    public function getExpirationDate(int $days): string
    {
        return now()->addDays($days)->toDateString();
    }

}
